#include <MozziGuts.h>
#include <Oscil.h>
#include <tables/sin2048_int8.h>
#include <Smooth.h>

#define CONTROL_RATE 150

Oscil <SIN2048_NUM_CELLS, AUDIO_RATE> aSin(SIN2048_DATA);

int frequency;
int frequency_adjusted;
int index;
int notes[25] = {220, 
                 233, 247, 262, 277, 294, 311, 330, 349, 370, 392, 415, 440, 466, 494,
                 523, 554, 587, 622, 659, 698, 740, 784, 831, 880};
int neardiff;
float gain;

void setup() {
  Serial.begin(115200);
  startMozzi(CONTROL_RATE);
}

void updateControl() {
  frequency = map(mozziAnalogRead(A0), 600, 780, 220, 880);
  frequency = autotune(frequency);
  gain = freq_adapt(frequency);
  aSin.setFreq(frequency);
}


int updateAudio() {
  return gain * aSin.next();
  //*gain;
}


void loop() {
  audioHook();
}

int autotune(int frequency) {
  frequency_adjusted = notes[0];
  neardiff = abs(notes[0] - frequency);
  for (int i = 1; i < 25; i++) {
    if (abs(notes[i] - frequency) < neardiff) {
      frequency_adjusted = notes[i];
      neardiff = abs(notes[i] - frequency);
    }
    else 
    { 
      break; 
    }
  }
  return frequency_adjusted;
}

float freq_adapt(int freq) {
  if (freq > 600) {
    return 0.4;
  }
  else if (freq > 400) {
    return 1;
  }
  else {
    return 1.4;
  }
}
